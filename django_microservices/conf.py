"""Settings for Django-Microservices."""
from appconf import AppConf
from django.conf import settings

__all__ = ('settings', 'DjangoMicroservicesConfig')


class DjangoMicroservicesConfig(AppConf):
    USER_SESSION_KEY = '_microservice_user'
    USER_PK_KEY = 'id'
    USER_PK_SESSION_KEY = '_microservice_user_pk'
    MICROSERVICE_AUTH_MICROSERVICE_NAME = 'api'
    AUTH_URL = 'api/usuarios/auth'
    USER_DATA_URL = 'api/usuarios/%s'
    HEADER_MICROSERVICE_USER_PK = 'MICROSERVICE-USER-PK'
    DJANGO_HEADER_MICROSERVICE_USER_PK = 'HTTP_MICROSERVICE_USER_PK'

    class Meta:
        proxy = True
        prefix = "MICROSERVICE"
