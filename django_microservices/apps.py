from django.apps import AppConfig


class DjangoMicroservicesConfig(AppConfig):
    name = 'django_microservices'
