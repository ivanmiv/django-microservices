import requests
from django.db import models
from requests.adapters import HTTPAdapter
from urllib3 import Retry
from .conf import settings


class MicroService(models.Model):
    name = models.CharField(max_length=100, unique=True)
    host = models.URLField(unique=True)

    @staticmethod
    def requests_retry_session(
            retries=3,
            backoff_factor=0.3,
            status_forcelist=(500, 502, 504),
            session=None,
    ):
        session = session or requests.Session()
        retry = Retry(
                total=retries,
                read=retries,
                connect=retries,
                backoff_factor=backoff_factor,
                status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def get_url(self, api):
        host = self.host if self.host[:-1] != '/' else self.host[:-1]
        url = "%s/%s" % (host, api)
        return url

    def remote_call(self, method, api='', params=None, data=None, json=None, headers=None, cookies=None, files=None,
                    auth=None, request=None):
        session = MicroService.requests_retry_session()
        api = self.get_url(api)

        extra_headers = {}

        if request and hasattr(request, 'META') and request.META:
            header_microservice_user_pk = request.META.get(settings.MICROSERVICE_DJANGO_HEADER_MICROSERVICE_USER_PK)
            if header_microservice_user_pk:
                extra_headers[settings.MICROSERVICE_HEADER_MICROSERVICE_USER_PK] = header_microservice_user_pk
        if request and hasattr(request, 'session') and extra_headers.get(settings.MICROSERVICE_HEADER_MICROSERVICE_USER_PK) is None:
            microservice_user_pk = str(request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY, None))
            if microservice_user_pk:
                extra_headers[settings.MICROSERVICE_HEADER_MICROSERVICE_USER_PK] = microservice_user_pk

        if headers and isinstance(headers,dict):
            extra_headers.update(headers)

        if method == 'GET':
            response = session.get(api, params=params, data=data, json=json, headers=extra_headers, cookies=cookies, files=files, auth=auth)
        elif method == 'POST':
            response = session.post(api, params=params, data=data, json=json, headers=extra_headers, cookies=cookies, files=files, auth=auth)
        elif method == 'PUT':
            response = session.put(api, params=params, data=data, json=json, headers=extra_headers, cookies=cookies, files=files, auth=auth)
        elif method == 'DELETE':
            response = session.delete(api, params=params, data=data, json=json, headers=extra_headers, cookies=cookies, files=files, auth=auth)
        else:
            raise NotImplementedError

        return response

    def get(self, api='', params=None, data=None, json=None, headers=None, cookies=None, files=None, auth=None, request=None):
        return self.remote_call('GET', api, params=params, data=data, json=json, headers=headers, cookies=cookies, files=files, auth=auth, request=request)

    def post(self, api='', params=None, data=None, json=None, headers=None, cookies=None, files=None, auth=None, request=None):
        return self.remote_call('POST', api, params=params, data=data, json=json, headers=headers, cookies=cookies, files=files, auth=auth, request=request)

    def put(self, api='', params=None, data=None, json=None, headers=None, cookies=None, files=None, auth=None, request=None):
        return self.remote_call('PUT', api, params=params, data=data, json=json, headers=headers, cookies=cookies, files=files, auth=auth, request=request)

    def delete(self, api='', params=None, data=None, json=None, headers=None, cookies=None, files=None, auth=None, request=None):
        return self.remote_call('DELETE', api, params=params, data=data, json=json, headers=headers, cookies=cookies, files=files, auth=auth, request=request)
