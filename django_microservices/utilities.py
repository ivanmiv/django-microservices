from django.middleware.csrf import rotate_token
from django.shortcuts import redirect

from .conf import settings
from .models import MicroService


def authenticate(username, password, username_key='username', password_key='password'):
    microservicio = MicroService.objects.get(name=settings.MICROSERVICE_AUTH_MICROSERVICE_NAME)
    respuesta = microservicio.remote_call(
            "POST", api=settings.MICROSERVICE_AUTH_URL, data={username_key: username, password_key: password}
    )
    if respuesta.status_code == 200:
        return respuesta.json()
    else:
        return None


def login(request, user):
    request.session[settings.MICROSERVICE_USER_PK_SESSION_KEY] = user[settings.MICROSERVICE_USER_PK_KEY]
    rotate_token(request)

def logout_session(request):
    if request.session.get(settings.MICROSERVICE_USER_SESSION_KEY):
        del request.session[settings.MICROSERVICE_USER_SESSION_KEY]

    if request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY):
        del request.session[settings.MICROSERVICE_USER_PK_SESSION_KEY]

    request.session.flush()

def logout(request):
    logout_session(request)


def login_required(function):
    def _inner(request, *args, **kwargs):
        if hasattr(request, "request"):
            request = request.request

        if not (hasattr(request, 'session') and request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY) and request.session.get(settings.MICROSERVICE_USER_SESSION_KEY)):
            return redirect(settings.LOGIN_URL)

        return function(request, *args, **kwargs)

    return _inner


def get_microservice_user(request):
    return request.session.get(settings.MICROSERVICE_USER_SESSION_KEY, {})


def get_microservice_user_pk(request):
    return request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY, None)
