from .conf import settings


def auth(request):
    if request.session.get(settings.MICROSERVICE_USER_SESSION_KEY):
        user = request.session.get(settings.MICROSERVICE_USER_SESSION_KEY)
    else:
        user = None

    return {
        'user': user,
    }
