from django.shortcuts import redirect

from .conf import settings
from .models import MicroService
from .utilities import logout_session

def authentication_middleware(get_response):
    def middleware(request):
        assert hasattr(request, 'session'), (
                                                "The Django microservices authentication middleware requires session middleware "
                                                "to be installed. Edit your MIDDLEWARE%s setting to insert "
                                                "'django.contrib.sessions.middleware.SessionMiddleware' before "
                                                "'django.contrib.auth.middleware.AuthenticationMiddleware'."
                                            ) % ("_CLASSES" if settings.MIDDLEWARE is None else "")
        if request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY):
            microservicio = MicroService.objects.get(name=settings.MICROSERVICE_AUTH_MICROSERVICE_NAME)
            try:
                respuesta = microservicio.remote_call(
                        "GET", api=settings.MICROSERVICE_USER_DATA_URL % request.session.get(settings.MICROSERVICE_USER_PK_SESSION_KEY)
                )
            except Exception:
                logout_session(request)
                return redirect(settings.LOGOUT_URL)

            if respuesta.status_code == 200:
                request.session[settings.MICROSERVICE_USER_SESSION_KEY] = respuesta.json()
            else:
                logout_session(request)
                return redirect(settings.LOGOUT_URL)

        response = get_response(request)
        return response

    return middleware
