django-microservices

About

My first Django package done for learning purposes

Installation

Install with pip from bitbucket

pip install git+https://bitbucket.org/ivanmiv/django-microservices.git


Configuration

Add the django_microservices to INSTALLED_APPS:


INSTALLED_APPS = [

    'django.contrib.admin',
	
    'django.contrib.auth',
	
    'django.contrib.contenttypes',
	
    'django.contrib.sessions',
	
    'django.contrib.messages',
	
    'django.contrib.staticfiles',
	
    'django_microservices',
    
]


File with the information of the microservices:

Example:
microservice.json

    [
        {
	
            "model": "microservices.microservice",
            
            "pk": 1,
            
            "fields": {
            
                "name": "user",
                
                "host": "http://user.example.com/",
                
        }    
    ]

Load file: 

    python manage.py loaddata microservice.json

