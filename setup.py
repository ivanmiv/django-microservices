from distutils.core import setup

setup(
    name='django_microservices',
    version='0.1.18',
    url='https://ivanmiv@bitbucket.org/ivanmiv/django-microservices',
    download_url='https://ivanmiv@bitbucket.org/ivanmiv/django-microservices.git',
    author='Ivan Viveros',
    author_email='ivanmviveros@hotmail.com',
    description='',
    packages=[
        'django_microservices',
        'django_microservices.migrations'
    ],
    install_requires=[
        'django-appconf>=0.6.0',
    ],
)
